# DateFormatter-2.0
# Auther:tianwenxiao
# mail:tianwenxiao@wo.cn
# 主要功能：
#         2.0：通过GUI选择要格式化的文件，在选择格式化后的文件存取路径，点击执行就可以统一将Excel中一列不同日期格式的文本统一格式化为YYYY-mm-dd这样的格式

import os
import subprocess
import tkinter as tk
from tkinter import filedialog
import pandas as pd
from datetime import datetime

def select_input_file(root,input_file_path_var):
    input_file_path = filedialog.askopenfilename(filetypes=[("Excel files", "*.xlsx;*.xls")])
    if input_file_path:
        input_file_path_var.delete(0, tk.END)
        input_file_path_var.insert(0, input_file_path)
        return input_file_path

def select_output_file(root,output_file_path_var):
    output_file_path = filedialog.asksaveasfilename(defaultextension=".xlsx")
    if output_file_path:
        output_file_path_var.delete(0,tk.END)
        output_file_path_var.insert(0, output_file_path)
        return output_file_path

# 定义一个函数来解析和格式化日期
def format_date(date_str):
    # 尝试多种可能的日期格式来解析字符串
    formats = ['%Y%m%d', '%Y.%m.%d', '%Y/%m/%d', '%Y-%m', '%Y/%m/%d','%Y.%m','%Y-%m-%d']
    for fmt in formats:
        try:
            # 解析日期字符串
            date = datetime.strptime(date_str, fmt)
            # 格式化为YYYY-MM-dd
            return date.strftime('%Y-%m-%d')
        except ValueError:
            pass  # 如果当前格式不匹配，则尝试下一个格式
    return None  # 如果所有格式都不匹配，则返回None

# 定义要执行的主要脚本
def do_format(root, input_file_path_var,output_file_path_var):
    inputfile = input_file_path_var.get()
    outputfile = output_file_path_var.get()
    df = pd.read_excel(inputfile, header=None)  # 假设数据在第一个工作表，且没有列标题
    # 应用函数到数据列的每一个元素
    df[0] = df[0].apply(format_date)
    # 将结果输出到一个新的Excel工作表中
    df.to_excel(outputfile, index=False, header=False)  # 不包含索引和列标题
    root.destroy()



# 创建主函数
def main():
    # 创建主窗口
    root = tk.Tk()
    root.title("日期格式化小工具")
    # 创建一个Tkinter变量来存储文件路径
    input_file_path_var = tk.StringVar()
    output_file_path_var = tk.StringVar()
    # 输入文件框
    input_frame = tk.Frame(root)
    input_frame.pack(pady=10)
    input_label = tk.Label(input_frame, text="输入文件:")
    input_label.pack(side=tk.LEFT)
    input_file_path_var = tk.Entry(input_frame, width=50)
    input_file_path_var.pack(side=tk.LEFT)
    input_button = tk.Button(input_frame, text="选择文件", command=lambda:select_input_file(root,input_file_path_var))
    input_button.pack(side=tk.LEFT)

    # 输出文件框
    output_frame = tk.Frame(root)
    output_frame.pack(pady=10)
    output_label = tk.Label(output_frame, text="输出文件:")
    output_label.pack(side=tk.LEFT)
    output_file_path_var = tk.Entry(output_frame, width=50)
    output_file_path_var.pack(side=tk.LEFT)
    output_button = tk.Button(output_frame, text="选择路径", command=lambda:select_output_file(root,output_file_path_var))
    output_button.pack(side=tk.LEFT)

    # 创建按钮用于执行脚本
    execute_button = tk.Button(root, text="执行脚本", command=lambda:do_format(root, input_file_path_var,output_file_path_var))
    execute_button.pack(pady=5)

    # 运行Tkinter事件循环
    root.mainloop()

if __name__ == "__main__":
    main()
