# DateFormatter-3.0
# Auther:tianwenxiao
# mail:tianwenxiao@wo.cn
# 主要功能：
#         2.0：通过GUI选择要格式化的文件，在选择格式化后的文件存取路径，点击执行就可以统一将Excel中一列不同日期格式的文本统一格式化为YYYY-mm-dd这样的格式
#         3.0：优化了2.0版本中只能将日期格式化为一种日期格式的不足，新增了YYYYmmdd、YYYY/mm/dd、YYYY.mm.dd、YYYY年mm月dd日等可选的格式，用户可以通过下拉
#              选项框来选择自己想要的格式
import os
import subprocess
import tkinter as tk
from tkinter import filedialog
from tkinter import ttk
from tkinter import messagebox
import pandas as pd
from datetime import datetime
selected_value = None
def select_input_file(root,input_file_path_var):
    input_file_path = filedialog.askopenfilename(filetypes=[("Excel files", "*.xlsx;*.xls")])
    if input_file_path:
        input_file_path_var.delete(0, tk.END)
        input_file_path_var.insert(0, input_file_path)
        return input_file_path

# 定义一个函数来处理选项变化事件
def on_combobox_change(selected_value):
    #print("选定的值:", selected_value.get())
    return selected_value.get()

# 定义一个函数来解析和格式化日期
def format_date(date_str):
    global selected_value
    # 获取用户选择的格式
    formatter_str = on_combobox_change(selected_value)
    # 尝试多种可能的日期格式来解析字符串
    formats = ['%Y%m%d', '%Y.%m.%d', '%Y/%m/%d', '%Y-%m', '%Y/%m/%d','%Y.%m','%Y-%m-%d','%Y年%m月%d日']
    for fmt in formats:
        try:
            # 解析日期字符串
            date = datetime.strptime(date_str, fmt)
            # 格式化为YYYY-MM-dd
            return date.strftime(formatter_str)
        except ValueError:
            pass  # 如果当前格式不匹配，则尝试下一个格式
    return None  # 如果所有格式都不匹配，则返回None

# 定义要执行的主要脚本
def do_format(root, input_file_path_var):
    try:
        inputfile = input_file_path_var.get()
        # 在源文件路径下生成
        outputfile = "".join(inputfile.split('.')[:-1]) + '_' + '格式化后的日期' + '.xlsx'
        df = pd.read_excel(inputfile, header=None)  # 假设数据在第一个工作表，且没有列标题
        # 应用函数到数据列的每一个元素
        df[0] = df[0].apply(format_date)
        # 将结果输出到一个新的Excel工作表中
        df.to_excel(outputfile, index=False, header=False)  # 不包含索引和列标题
        messagebox.showinfo("结果","恭喜您，所有的日期均已格式化完成，请移步{}查看。".format(outputfile))
        root.destroy()
    except Exception as e:
        messagebox.showinfo("警告","出现了错误，请检查源文件格式，然后重试")



# 创建主函数
def main():
    # 创建主窗口
    root = tk.Tk()
    root.title("日期格式化小工具")
    # 创建一个Tkinter变量来存储文件路径
    input_file_path_var = tk.StringVar()
    output_file_path_var = tk.StringVar()

    # 创建一个字符串变量来存储选定的值
    global selected_value
    selected_value = tk.StringVar()

    # 定义下拉选项框的选项
    options = ["%Y-%m-%d", "%Y/%m/%d", "%Y.%m.%d","%Y年%m月%d日", "%Y%m%d","%Y%m","%Y年%m月"]
    # 输入文件框
    input_frame = tk.Frame(root)
    input_frame.pack(pady=10)
    input_label = tk.Label(input_frame, text="输入文件:")
    input_label.pack(side=tk.LEFT)
    input_file_path_var = tk.Entry(input_frame, width=50)
    input_file_path_var.pack(side=tk.LEFT)
    input_button = tk.Button(input_frame, text="选择文件", command=lambda:select_input_file(root,input_file_path_var))
    input_button.pack(side=tk.LEFT)
    # 创建下拉选项框
    combobox_frame = tk.Frame(root)
    combobox_frame.pack(pady=10)
    combobox_label = tk.Label(combobox_frame, text="选择目标格式:")
    combobox_label.pack(side=tk.LEFT)
    combobox = ttk.Combobox(combobox_frame, textvariable=selected_value, values=options)
    combobox.pack()

    # 设置默认选项
    combobox.set(options[0])
    # 绑定选项变化事件处理函数
    selected_value.trace_add("write",lambda *args: on_combobox_change(selected_value))
    # 创建按钮用于执行脚本
    execute_button = tk.Button(root, text="执行脚本", command=lambda:do_format(root, input_file_path_var))
    execute_button.pack(pady=5)

    # 运行Tkinter事件循环
    root.mainloop()

if __name__ == "__main__":
    main()
