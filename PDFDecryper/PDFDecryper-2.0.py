# PDFDecryper-2.0
# Auther:tianwenxiao
# mail:tianwenxiao@wo.cn
# 主要功能:针对没有设置打开密码的PDF文件，只是设置了打印、编辑等限制的密码，可以使用该脚本进行解密
import os
import tkinter as tk
from tkinter import filedialog
#pdf的读取方法
from PyPDF2 import PdfReader
#pdf的写入方法
from PyPDF2 import PdfWriter
#高加密的方法，要引入不然会报错
from Crypto.Cipher import AES
# 该脚本使用Python3

def select_input_file(root, input_file_path_var):
    input_file_path = filedialog.askopenfilename(filetypes=[("PDF files", "*.PDF;*.pdf")])
    if input_file_path:
        input_file_path_var.delete(0, tk.END)
        input_file_path_var.insert(0, input_file_path)
    return input_file_path


# 定义一个函数来读取PDF文件
def get_reader(filename, password):
    try:
        old_file = open(filename, 'rb')
        #print('解密开始...')
    except Exception as err:
        return print('文件打开失败！' + str(err))

    #如果使用python2需要将PdfReader改为PdfFileReader
    pdf_reader = PdfReader(old_file, strict=False)

    # 如果使用python2需要将is_encrypted改为isEncrypted
    # 执行解密操作
    if pdf_reader.is_encrypted:
        if password is None:
            return print('文件被加密，需要密码！--{}'.format(filename))
        else:
            if pdf_reader.decrypt(password) != 1:
                return print('密码不正确！--{}'.format(filename))
    elif old_file in locals():
        old_file.close()
        # 返回结果
    return pdf_reader

def deception_pdf(root,filename, password, decrypted_filename=None):
    #print('正在生成解密...')
    pdf_reader = get_reader(filename, password)
    if pdf_reader is None:
        return print("无内容读取")

    # 如果使用python2需要将is_encrypted改为isEncrypted
    elif not pdf_reader.is_encrypted:
        return print('文件没有被加密，无需操作')

    # 如果使用的是python2需要将PdfWriter改为PdfFileWriter
    pdf_writer = PdfWriter()

    #如果使用的是python2需要将将append_pages_from_reader改为appendPagesFromReader
    pdf_writer.append_pages_from_reader(pdf_reader)
    #创建解密后的pdf文件和展示文件的路径
    if decrypted_filename is None:
        decrypted_filename = "".join(filename.split('.')[:-1]) + '_' + '已解密' + '.pdf'
        print("解密文件已生成:{}".format(decrypted_filename))
    # 写入新文件
    pdf_writer.write(open(decrypted_filename, 'wb'))
    root.destroy()

# 创建主函数
def main():
    # 创建主窗口
    root = tk.Tk()
    root.title("PDF解密小工具")
    # 创建一个Tkinter变量来存储文件路径
    input_file_path_var = tk.StringVar()
    # 输入文件框
    input_frame = tk.Frame(root)
    input_frame.pack(pady=10)
    input_label = tk.Label(input_frame, text="输入文件:")
    input_label.pack(side=tk.LEFT)
    input_file_path_var = tk.Entry(input_frame, width=50)
    input_file_path_var.pack(side=tk.LEFT)
    input_button = tk.Button(input_frame, text="选择文件", command=lambda:select_input_file(root,input_file_path_var))
    input_button.pack(side=tk.LEFT)

    # 创建按钮用于执行脚本
    execute_button = tk.Button(root, text="执行脚本", command=lambda:deception_pdf(root,input_file_path_var.get(), ''))
    execute_button.pack(pady=5)

    # 运行Tkinter事件循环
    root.mainloop()

if __name__ == "__main__":
    main()
